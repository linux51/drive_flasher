#!/bin/bash

export http_proxy=http://openproxy.bmstu.ru:3128/

# apt-get update
# apt-get install rp-pppoe-gui

pppoe-setup

cp -v pppoestart /usr/bin
cp -v pppoestop /usr/bin

gpasswd -d user wheel
chmod +x /usr/bin/sudo

cp -v sus/pppoe /etc/sudoers.d
cp -v sus/su /etc/sudoers.d

cp -v icon/start.desktop /home/user/Рабочий\ стол
cp -v icon/stop.desktop /home/user/Рабочий\ стол

cp -v icon/.start_script.sh /home/user/Документы
cp -v icon/.stop_script.sh /home/user/Документы

