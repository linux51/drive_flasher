#! /bin/bash

timedatectl set-ntp yes

detectw=$(lsblk -f | awk '/ntfs/')
freespasewin=50000 ##free spase in new Win partition
minspaselin=250000 ##minimal spase for Linux partition
minminspaselin=100000 ##minimal minimal spase for Linux partition
if [ -z "$detectw" ]
  then
    echo -en "\033[92mOS Windows not detected\033[0m\n"
  else
    diskw=$(lsblk -f|grep ntfs|cut -c 3-|cut -c -3|awk 'NR==1{print $1}')
    echo -en "\033[91mDetected Windows partition on disk" $diskw "\033[0m\n"
    lsblk -f|grep ntfs| cut -c 3-
    bootnow=$(lsblk -l|grep boot|cut -c -3)
    echo -en "\033[96mNow system boot on disk" $bootnow "\033[0m\n"
    mkdir -p /mnt/tmp2
    for i in $(ls /dev|grep $diskw|awk 'FNR>1')
    do
    mkdir /mnt/tmp2/$i
    mount /dev/$i /mnt/tmp2/$i 2>/dev/null
        if [[ "$(ls -d /mnt/tmp2/$i/*/ 2>/dev/null|grep -iE efi)" ]] && ! [[ "$(ls -d /mnt/tmp2/$i/*/ 2>/dev/null|grep -iE recovery)" ]]; then
        echo -en "\033[96mEFI detected on "$i"\033[0m\n"
        efipart=$i
        fi
    umount /mnt/tmp2/$i 2>/dev/null
    rmdir /mnt/tmp2/$i
    done
    rmdir /mnt/tmp2
fi
lsblk
echo -----------
fdisk -l | grep "Disk model" -B 1
DISTR_VERSION=$(ls /archive | grep alt-system | grep -Eo '[0-9].[0-9].[0-9]{1,4}')
echo "Run installation" $(ls /archive | grep alt-system | grep -Eo '[0-9].[0-9].[0-9]{1,4}')"? yes/no"
read install
if [[ "$install" == "yes" || "$install" == "y" ]]; then
  echo "Install Dual boot? yes/no"
  read dualboot
    if [[ "$dualboot" == "yes" || "$dualboot" == "y" ]]; then
        dual=1
        if [[ -d "/sys/firmware/efi" && -n "$efipart" ]]; then :
        else
            if [[ ! -d "/sys/firmware/efi" && -z "$efipart" ]]; then :
            else
                echo -en "\033[31mInstalled Windows and bootable USB flash drive in different modes (UEFI/Legacy)\033[0m\n"
                exit 1
            fi
        fi
    else dual=0; fi
  echo "Select the drive to install or Windows partition to install Dual boot on one hard disk. Example sda,sdb,sdc or sda2,sdb3,sdc1...."
  read DISK
     if [[ "$diskw" == "$DISK" ]]; then
         echo -en "\033[91mYou want install linux on Windows disk!""\033[0m\n"
        exit 1
    fi
    if [[ "${#DISK}" -gt "3" ]]; then ddpt=${DISK::-1}; else ddpt=${DISK}; fi
    dpt=$(parted /dev/${ddpt} print|grep 'Partition Table'|awk '{print $3}')
    if [[ "$dpt" == "msdos" && "$dual" == "1" ]];then
      wt="7"
      lt="83"
    elif [[ "$dpt" == "gpt" && "$dual" == "1" ]];then
      wt="11"
      lt="20"
    elif [[ "$dual" == "0" ]];then :
    else
      echo -en "\033[31mPartition table unknown\033[0m\n"
      exit 1
    fi
    numwinpart=${DISK:3}
    if [[ "${#DISK}" -gt "3" && ${DISK::-1} -eq ${diskw} ]]; then
        pwinpart=$(parted /dev/${DISK::-1} print|grep primary|wc -l)
        echo -en "\033[91mYou want install Linux on Windows partition.""\033[0m\n"
        if [[ ! -d '/sys/firmware/efi' ]]; then
            if [[ "$dual" -eq "1" && $pwinpart -gt "3" ]]; then
                echo -en "\033[91mDisc has more than 3 primary partitions. installation is not possible!""\033[0m\n"
                exit 1
            fi
        fi
        ##################
        dualwp=1
        else
        dualwp=0
    fi
    sizesdw=$(($(lsblk -b --output SIZE -n -d /dev/${DISK}) / 1048576)) ##Win part size
    minwinpart=$(ntfsresize -i -f /dev/${DISK}|grep "You might resize"|awk '{ print $8 }') ##minimal size win part
    newsizew=$(($minwinpart + $freespasewin)) ##new size Win part
    newlinpart=$(($sizesdw - $newsizew - "1000")) ##size new Linux part

    if [[ "$dualwp" = "1" ]]; then
    echo "Size new Linux partition:" $newlinpart "MB"
        if [[ "$newlinpart" -gt "$minspaselin" ]]; then
        newlinpart=$minspaselin
        elif [[ "$newlinpart" -lt "$minspaselin" && "$newlinpart" -gt "$minminspaselin" ]]; then
        newlinpart=$minminspaselin
        else
            echo "New partition for Linux is too small"
            exit 1
        fi
        newsizew=$(($sizesdw-$newlinpart-"1000"))
        newlinpartM=${newlinpart}"M"
        newsizewM=${newsizew}"M"
    fi
    echo "Select XFCE for default? yes/no"
    read DE_answer
    if [[ $DE_answer == "yes" || $DE_answer == "y" ]]; then
        DE=xfce
    elif [[ $DE_answer == "no" || $DE_answer == "n" ]]; then
        DE=plasma
    else
    echo "Incorrect answer"
    exit 1
  fi
  echo "Checking /archive/$(ls /archive | grep alt-system)"
  pv /archive/$(ls /archive | grep alt-system) | tar -I zstdmt -t   > /dev/null ### disable for tests
  CHECK_TAR=$?
  if [[ $CHECK_TAR = 0 ]]; then
    if [ -d '/sys/firmware/efi' ]
    then
      DIALOG_HOSTNAME=${DIALOG_HOSTNAME=dialog}
      tempfile=/tmp/hostname_tmp
      trap "rm -f $tempfile" 0 1 2 5 15
      $DIALOG_HOSTNAME  --title "Hostname" --clear \
                        --inputbox "Input hostname:" 20 61 2> $tempfile
      retval_hostname=$?
      DIALOG_SOFT=${DIALOG_SOFT=dialog}
      tempfile=`mktemp 2>/dev/null` || tempfile=/tmp/test$$
      trap "rm -f $tempfile" 0 1 2 5 15
      $DIALOG_SOFT --backtitle "Alt linux install" \
              --title "Soft installation" --clear \
              --checklist "Choose soft for install" 20 61 5 \
              "msoffice2010" "Install Microsoft office 2010" off \
              "wine_altium" "Altium Designer" off \
              "wine_apm" "APM WinMachine" off \
              "wine_catia" "CATIA" off \
              "wine_kompas" "Kompas v20" off \
              "mathcad14" "Mathcad 2014" off \
              "wine_mathcad" "Mathcad 2015" off \
              "wine_multisim" "NI Multisim" off \
              "wine_solidworks" "SolidWorks" off \
              "mathematica"  "Mathematica" off \
              "matlab"    "Matlab" off \
              "matlab2012" "matlab 2012" off \
              "salomemeca"    "SalomeMeca" off \
              "siemensnx"    "SiemensNX" off \
              "labview"   "labview install" off \
              "techsoft"    "techsoft" off \
              "quartus"    "quartus" off \
              "ansys2021" "Ansys 2021" off \
              "stm32cubdeide" "STM32CubdeIDE" off \
              "xilinx"   "Xilinx" off  2> $tempfile
      retval_soft=$?
      choice=`cat $tempfile`
      if [[ "$dualwp" = "1" ]]; then ### start dual one hard UEFI
        ntfsresize -b --size ${newsizew}M /dev/${DISK} <<EOF
y
EOF
        fdisk /dev/${DISK::-1} <<EOF
d
$numwinpart
n
$numwinpart

+$newsizewM
y
t
$numwinpart
$wt
n


+1000M
t

$lt
n



t

$lt
w
EOF
      bootp=$( (fdisk -l|grep 1000M|grep Linux|awk 'NR==1{print $1}'))
      bootp2=$( (fdisk -l|grep Linux|grep ${DISK::-1}|awk '/./{line=$0} END{print line}'|awk '{print $1}'))
      mkfs.ext2 -F $bootp
      mkfs.ext4 -F $bootp2
      mount $bootp2 /mnt
      mkdir /mnt/boot
      mount $bootp /mnt/boot
      echo "Unarchive /archive/$(ls /archive | grep alt-system)"
      pv /archive/$(ls /archive/ | grep system | sort | tail -n 1) | tar -I zstdmt --numeric-owner -xp -C /mnt/
      sed -i "/Session/c Session=$DE" /mnt/etc/X11/sddm/sddm.conf
      sed -i "s/GRUB_GFXMODE='800x600'/GRUB_GFXMODE='text'/" /mnt/etc/sysconfig/grub2
      sed -i "s/GRUB_TERMINAL_OUTPUT='gfxterm'/GRUB_TERMINAL_OUTPUT='console'/" /mnt/etc/sysconfig/grub2
      sed -i 's/CLASS \-\-class os \\/CLASS \-\-class os \-\-unrestricted \\/' /mnt/etc/grub.d/30_os-prober
      for i in $choice; do pv /archive/$i.tar.zst | tar -I zstdmt --numeric-owner -xp -C /mnt/ ; done
      HOSTNAME=$(cat /tmp/hostname_tmp)
      echo -e '127.0.0.1  localhost.localdomain localhost\n127.0.0.1 '$HOSTNAME > /mnt/etc/hosts
      echo $HOSTNAME > /mnt/etc/hostname
      touch /etc/bmstu-$(ls /archive/ | grep alt-system | grep -Eo '[0-9].[0-9].[0-9]{1,4}')
         mkdir -p /mnt/proc /mnt/dev/ /mnt/sys /mnt/tmp /mnt/boot/efi /mnt/sys/firmware/efi/efivars
         mount /dev/$efipart /mnt/boot/efi
         for i in /dev /dev/pts /proc /sys /sys/firmware/efi/efivars /run; do mount -B $i /mnt$i; done
         echo -e "#!/bin/bash\n
              efibootmgr
              grub-install --target=x86_64-efi  --bootloader-id=altlinux --recheck
              os-prober
              grub-mkconfig -o /boot/grub/grub.cfg
              chown -R root:root /opt/soft 2>/dev/null
              chmod -R 755 /opt/soft 2>/dev/null
              chown -R 500:500 /home/user
              chown -R 501:502 /home/develop
              chown -R 468:501 /home/usr1cv8
              chmod -R 666 /opt/soft/advego_plagiatus_3/*.log
              if [[ $(free -m | awk 'NR == 2{print $2}') -lt 5000 ]]; then
                fallocate -l $(free -m | awk 'NR == 2{print $2}')M /swapfile
                chmod 600 /swapfile
                /sbin/mkswap /swapfile
                /sbin/swapon /swapfile
                echo '/swapfile swap swap defaults 0 0' >> /etc/fstab
              fi
              " > /mnt/tmp/grub.sh
      chmod +x /mnt/tmp/grub.sh
      genfstab -U -p /mnt > /mnt/etc/fstab
      chroot /mnt /usr/bin/zsh /tmp/grub.sh
      wget -P /patch -A zst -m -p -E -k -K -npd -t 2 -T 5 http://rpl.bmstu.ru/patch/
      for i in $(ls /patch); do pv /patch/$i | tar -I zstdmt -xp -C /mnt/;done
      umount -l /dev/$efipart $bootp $bootp2
      else ##end dual one hard UEFI
      parted -a optimal /dev/${DISK} --script mklabel gpt
      parted -a optimal /dev/${DISK} --script mkpart "efi" fat32 1MiB 261MiB
      parted -a optimal /dev/${DISK} --script mkpart "boot" ext2 261MiB 1261MiB
      parted -a optimal /dev/${DISK} --script mkpart "root" ext4 1261MiB 100%
      parted --script /dev/${DISK} set 1 esp on
      parted --script /dev/${DISK} set 1 boot on
      mkfs.fat -F32 /dev/${DISK}1
      mkfs.ext2 -F /dev/${DISK}2
      mkfs.ext4 -F /dev/${DISK}3
      mount /dev/${DISK}3 /mnt
      mkdir /mnt/boot
      mount /dev/${DISK}2 /mnt/boot
      echo "Unpacking /archive/$(ls /archive | grep alt-system)"
      pv /archive/$(ls /archive/ | grep system | sort | tail -n 1) | tar -I zstdmt --numeric-owner -xp -C /mnt/
      sed -i "/Session/c Session=$DE" /mnt/etc/X11/sddm/sddm.conf
      sed -i "s/GRUB_GFXMODE='800x600'/GRUB_GFXMODE='text'/" /mnt/etc/sysconfig/grub2
      sed -i "s/GRUB_TERMINAL_OUTPUT='gfxterm'/GRUB_TERMINAL_OUTPUT='console'/" /mnt/etc/sysconfig/grub2
      sed -i 's/CLASS \-\-class os \\/CLASS \-\-class os \-\-unrestricted \\/' /mnt/etc/grub.d/30_os-prober
      for i in $choice; do echo "Unpacking $i" && pv /archive/$i.tar.zst | tar -I zstdmt --numeric-owner -xp -C /mnt/ ; done
      HOSTNAME=$(cat /tmp/hostname_tmp)
      echo -e '127.0.0.1  localhost.localdomain localhost\n127.0.0.1 '$HOSTNAME > /mnt/etc/hosts
      echo $HOSTNAME > /mnt/etc/hostname
      touch /mnt/etc/bmstu-$(ls /archive/ | grep alt-system | grep -Eo '[0-9].[0-9].[0-9]{1,4}')
       if [[ "$dual" = "1" ]]; then ## Dual boot UEFI start block
         mkdir -p /mnt/proc /mnt/dev/ /mnt/sys /mnt/tmp /mnt/boot/efi /mnt/sys/firmware/efi/efivars
         mount /dev/$efipart /mnt/boot/efi
         for i in /dev /dev/pts /proc /sys /sys/firmware/efi/efivars /run; do mount -B $i /mnt$i; done
         echo -e "#!/bin/bash\n
              efibootmgr
              grub-install --target=x86_64-efi  --bootloader-id=altlinux --recheck
              os-prober
              grub-mkconfig -o /boot/grub/grub.cfg
              chown -R root:root /opt/soft 2>/dev/null
              chmod -R 755 /opt/soft 2>/dev/null
              chown -R 500:500 /home/user
              chown -R 501:502 /home/develop
              chown -R 468:501 /home/usr1cv8
                    chmod -R 666 /opt/soft/advego_plagiatus_3/*.log
              if [[ $(free -m | awk 'NR == 2{print $2}') -lt 5000 ]]; then
                fallocate -l $(free -m | awk 'NR == 2{print $2}')M /swapfile
                chmod 600 /swapfile
                /sbin/mkswap /swapfile
                /sbin/swapon /swapfile
                echo '/swapfile swap swap defaults 0 0' >> /etc/fstab
              fi
              " > /mnt/tmp/grub.sh
     chmod +x /mnt/tmp/grub.sh
     genfstab -U -p /mnt > /mnt/etc/fstab
     chroot /mnt /usr/bin/zsh /tmp/grub.sh
     wget -P /patch -A zst -m -p -E -k -K -npd -t 2 -T 5 http://rpl.bmstu.ru/patch/
     for i in $(ls /patch); do pv /patch/$i | tar -I zstdmt -xp -C /mnt/;done
     umount -l /dev/$efipart /dev/${DISK}2 /dev/${DISK}3 ## Dual boot UEFI end block
       else
     mkdir -p /mnt/proc /mnt/dev/ /mnt/sys /mnt/tmp /mnt/boot/efi /mnt/sys/firmware/efi/efivars
         mount /dev/${DISK}1 /mnt/boot/efi
         for i in /dev /dev/pts /proc /sys /sys/firmware/efi/efivars /run; do mount -B $i /mnt$i; done
     echo -e "#!/bin/bash\n
              efibootmgr
              grub-install --target=x86_64-efi  --efi-directory=/boot/efi --bootloader-id=altlinux --recheck
              grub-mkconfig -o /boot/grub/grub.cfg
              chown -R root:root /opt/soft 2>/dev/null
              chmod -R 755 /opt/soft 2>/dev/null
              chown -R 500:500 /home/user
              chown -R 501:502 /home/develop
              chown -R 468:501 /home/usr1cv8
              if [ -d '/opt/soft/advego_plagiatus_3' ]; then
                chmod -R 666 /opt/soft/advego_plagiatus_3/*.log 2>/dev/null
              fi
              if [[ $(free -m | awk 'NR == 2{print $2}') -lt 5000 ]]; then
                fallocate -l $(free -m | awk 'NR == 2{print $2}')M /swapfile
                chmod 600 /swapfile
                /sbin/mkswap /swapfile
                /sbin/swapon /swapfile
                echo '/swapfile swap swap defaults 0 0' >> /etc/fstab
              fi
              " > /mnt/tmp/grub.sh
     chmod +x /mnt/tmp/grub.sh
     genfstab -U -p /mnt > /mnt/etc/fstab
     chroot /mnt /usr/bin/zsh /tmp/grub.sh
     wget -P /patch -A zst -m -p -E -k -K -npd -t 2 -T 5 -e robots=off -R index.html* http://rpl.bmstu.ru/patch/
      for i in $(ls /patch | grep $DISTR_VERSION); do pv /patch/$i | tar -I zstdmt -xp -C /mnt/;done
     umount -l /dev/${DISK}1 /dev/${DISK}2 /dev/${DISK}3
     sh /root/get-info.sh
     fi
     fi
    else
      DIALOG_HOSTNAME=${DIALOG_HOSTNAME=dialog}
      tempfile=/tmp/hostname_tmp
      trap "rm -f $tempfile" 0 1 2 5 15
      $DIALOG_HOSTNAME  --title "Hostname" --clear \
                        --inputbox "Input hostname:" 20 61 2> $tempfile
      retval_hostname=$?
      DIALOG_SOFT=${DIALOG_SOFT=dialog}
      tempfile=`mktemp 2>/dev/null` || tempfile=/tmp/test$$
      trap "rm -f $tempfile" 0 1 2 5 15
      $DIALOG_SOFT --backtitle "Alt linux install" \
              --title "Soft installation" --clear \
              --checklist "Choose soft for install" 20 61 5 \
              "msoffice2010" "Install Microsoft office 2010" off \
              "wine_altium" "Altium Designer" off \
              "wine_apm" "APM WinMachine" off \
              "wine_catia" "CATIA" off \
              "wine_kompas" "Kompas v20" off \
              "mathcad14" "Mathcad 2014" off \
              "wine_mathcad" "Mathcad 2015" off \
              "wine_multisim" "NI Multisim" off \
              "wine_solidworks" "SolidWorks" off \
              "mathematica"  "Mathematica" off \
              "matlab"    "Matlab" off \
              "matlab2012" "matlab 2012" off \
              "salomemeca"    "SalomeMeca" off \
              "siemensnx"    "SiemensNX" off \
              "labview"   "labview install" off \
              "techsoft"    "techsoft" off \
              "quartus"    "quartus" off \
              "ansys2021" "Ansys 2021" off \
              "stm32cubdeide" "STM32CubdeIDE" off \
              "xilinx"   "Xilinx" off  2> $tempfile
      retval_soft=$?
      choice=`cat $tempfile`
      if [[ "$dualwp" = "1" ]]; then ### start dual one hard legacy
ntfsresize -b --size ${newsizew}M /dev/${DISK} <<EOF
y
EOF
fdisk /dev/${DISK::-1} <<EOF
d
$numwinpart
n
p
$numwinpart

+$newsizewM
y
t
$numwinpart
$wt
n
p



t


$lt
w
EOF
      linp=$( (fdisk -l|grep ${DISK::-1}|grep Linux|awk 'NR==1{print $1}'))
      mkfs.ext4 -F $linp
      mount $linp /mnt
      echo "Unarchive /archive/$(ls /archive | grep alt-system)"
      pv /archive/$(ls /archive/ | grep system | sort | tail -n 1) | tar -I zstdmt --numeric-owner -xp -C /mnt/
      sed -i "/Session/c Session=$DE" /mnt/etc/X11/sddm/sddm.conf
      sed -i "s/GRUB_GFXMODE='800x600'/GRUB_GFXMODE='text'/" /mnt/etc/sysconfig/grub2
      sed -i "s/GRUB_TERMINAL_OUTPUT='gfxterm'/GRUB_TERMINAL_OUTPUT='console'/" /mnt/etc/sysconfig/grub2
      sed -i 's/CLASS \-\-class os \\/CLASS \-\-class os \-\-unrestricted \\/' /mnt/etc/grub.d/30_os-prober
      for i in $choice; do pv /archive/$i.tar.zst | tar -I zstdmt --numeric-owner -xp -C /mnt/ ; done
      mkdir -p /mnt/proc /mnt/dev/ /mnt/sys /mnt/tmp
      HOSTNAME=$(cat /tmp/hostname_tmp)
      echo -e '127.0.0.1  localhost.localdomain localhost\n127.0.0.1 '$HOSTNAME > /mnt/etc/hosts
      echo $HOSTNAME > /mnt/etc/hostname
      touch /etc/bmstu-$(ls /archive/ | grep alt-system | grep -Eo '[0-9].[0-9].[0-9]{1,4}')
        echo -e "#!/bin/bash\n
              grub-install /dev/${diskw}
              os-prober
              grub-mkconfig -o /boot/grub/grub.cfg
              chown -R root:root /opt/soft 2>/dev/null
              chmod -R 755 /opt/soft 2>/dev/null
              chown -R 500:500 /home/user
              chown -R 501:502 /home/develop
              chown -R 468:501 /home/usr1cv8
                    chmod -R 666 /opt/soft/advego_plagiatus_3/*.log
              if [[ $(free -m | awk 'NR == 2{print $2}') -lt 5000 ]]; then
                fallocate -l $(free -m | awk 'NR == 2{print $2}')M /swapfile
                chmod 600 /swapfile
                /sbin/mkswap /swapfile
                /sbin/swapon /swapfile
                echo '/swapfile swap swap defaults 0 0' >> /etc/fstab
              fi
              " > /mnt/tmp/grub.sh
      chmod +x /mnt/tmp/grub.sh
      mount -t proc /proc /mnt/proc/
      mount --rbind /sys /mnt/sys/
      mount --rbind /dev /mnt/dev/
      genfstab -U -p /mnt > /mnt/etc/fstab
      chroot /mnt /usr/bin/zsh /tmp/grub.sh
      wget -P /patch -A zst -m -p -E -k -K -npd -t 2 -T 5 http://rpl.bmstu.ru/patch/
      for i in $(ls /patch); do pv /patch/$i | tar -I zstdmt -xp -C /mnt/;done
      umount -l $linp
else ##end block of one hard legacy
      parted -a optimal /dev/${DISK} --script mklabel msdos
      parted -a optimal /dev/${DISK} --script mkpart primary ext4 0% 100%
      parted --script /dev/${DISK} set 1 boot on
      mkfs.ext4 -F /dev/${DISK}1
      mount /dev/${DISK}1 /mnt
      echo "Unarchive /archive/$(ls /archive | grep alt-system)"
      pv /archive/$(ls /archive/ | grep system | sort | tail -n 1) | tar -I zstdmt --numeric-owner -xp -C /mnt/
      sed -i "/Session/c Session=$DE" /mnt/etc/X11/sddm/sddm.conf
      sed -i "s/GRUB_GFXMODE='800x600'/GRUB_GFXMODE='text'/" /mnt/etc/sysconfig/grub2
      sed -i "s/GRUB_TERMINAL_OUTPUT='gfxterm'/GRUB_TERMINAL_OUTPUT='console'/" /mnt/etc/sysconfig/grub2
      sed -i 's/CLASS \-\-class os \\/CLASS \-\-class os \-\-unrestricted \\/' /mnt/etc/grub.d/30_os-prober
      for i in $choice; do echo "Unpacking $i" && pv /archive/$i.tar.zst | tar -I zstdmt --numeric-owner -xp -C /mnt/ ; done
      mkdir -p /mnt/proc /mnt/dev/ /mnt/sys /mnt/tmp
      HOSTNAME=$(cat /tmp/hostname_tmp)
      echo -e '127.0.0.1  localhost.localdomain localhost\n127.0.0.1 '$HOSTNAME > /mnt/etc/hosts
      echo $HOSTNAME > /mnt/etc/hostname
      touch /mnt/etc/bmstu-$(ls /archive/ | grep alt-system | grep -Eo '[0-9].[0-9].[0-9]{1,4}')
       if [[ "$dual" = "1" ]]; then ## Dual boot legacy start block
        echo -e "#!/bin/bash\n
              grub-install /dev/${diskw}
              os-prober
              grub-mkconfig -o /boot/grub/grub.cfg
              chown -R root:root /opt/soft 2>/dev/null
              chmod -R 755 /opt/soft 2>/dev/null
              chown -R 500:500 /home/user
              chown -R 501:502 /home/develop
              chown -R 468:501 /home/usr1cv8
                    chmod -R 666 /opt/soft/advego_plagiatus_3/*.log
              if [[ $(free -m | awk 'NR == 2{print $2}') -lt 5000 ]]; then
                fallocate -l $(free -m | awk 'NR == 2{print $2}')M /swapfile
                chmod 600 /swapfile
                /sbin/mkswap /swapfile
                /sbin/swapon /swapfile
                echo '/swapfile swap swap defaults 0 0' >> /etc/fstab
              fi
              " > /mnt/tmp/grub.sh  ## Dual boot legacy end block
       else
        echo -e "#!/bin/bash\n
              grub-install /dev/${DISK}
              grub-mkconfig -o /boot/grub/grub.cfg
              chown -R root:root /opt/soft 2>/dev/null
              chmod -R 755 /opt/soft 2>/dev/null
              chown -R 500:500 /home/user
              chown -R 501:502 /home/develop
              chown -R 468:501 /home/usr1cv8
              if [ -d '/opt/soft/advego_plagiatus_3' ]; then
                chmod -R 666 /opt/soft/advego_plagiatus_3/*.log 2>/dev/null
              fi
              if [[ $(free -m | awk 'NR == 2{print $2}') -lt 5000 ]]; then
                fallocate -l $(free -m | awk 'NR == 2{print $2}')M /swapfile
                chmod 600 /swapfile
                /sbin/mkswap /swapfile
                /sbin/swapon /swapfile
                echo '/swapfile swap swap defaults 0 0' >> /etc/fstab
              fi
              " > /mnt/tmp/grub.sh
       fi
      chmod +x /mnt/tmp/grub.sh
      mount -t proc /proc /mnt/proc/
      mount --rbind /sys /mnt/sys/
      mount --rbind /dev /mnt/dev/
      genfstab -U -p /mnt > /mnt/etc/fstab
      chroot /mnt /usr/bin/zsh /tmp/grub.sh
      wget -P /patch -A zst -m -p -E -k -K -npd -t 2 -T 5 -e robots=off -R index.html* http://rpl.bmstu.ru/patch/
      for i in $(ls /patch | grep $DISTR_VERSION); do pv /patch/$i | tar -I zstdmt -xp -C /mnt/;done
      umount -l /dev/${DISK}1 
      sh /root/get-info.sh 
    fi
    fi
  elif [[ $CHECK_TAR != 0 ]]; then
    echo "/archive/$(ls /archive | grep alt-system) is broken"
    exit 1
  fi
elif [[ "$install" == "no" ]]; then
  exit 1
fi
