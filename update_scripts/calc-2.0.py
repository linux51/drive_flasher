import os
import re
import pprint


bts = {'alt-system-2.0.0.tar.zst': 10010318233,
       'ansys2021.tar.zst': 14716018241,
       'labview.tar.zst': 498185019, 
       'mathcad14.tar.zst': 786953480,
       'mathematica.tar.zst': 3797033552, 
       'matlab.tar.zst': 13297394191, 
       'quartus.tar.zst': 4853027531,
       'salomemeca.tar.zst': 2114968875, 
       'siemensnx.tar.zst': 6380460069,
       'stm32cubdeide.tar.zst': 847326097,
       'techsoft.tar.zst': 12631908668, 
       'xilinx.tar.zst': 8082416762,
       'wine_altium.tar.zst': 1658514751,
       'wine_apm.tar.zst': 7034530545,
       'wine_catia.tar.zst': 10147758647,
       'wine_kompas.tar.zst': 2424458366,
       'wine_mathcad.tar.zst': 764173305,
       'wine_multisim.tar.zst': 6820048317,
       'wine_solidworks.tar.zst': 9536663847}

class calculon():
    def __init__(self):
        self.origin_dict = bts
        self.main_dict = {}
        self.no_misobs = False
        self.no_error = False
        self.dw_path = f"/home/user31/Downloads/soft/"

    def read_info(self):
        for filename in os.listdir('./'):
            if filename[-4:] == '.zst':
                cmd = f'stat --format="%s" {filename}'
                self.main_dict[filename] = int(os.popen(cmd).read())


    def val_num(self):
        dir_list = os.popen('ls').read()
        pattern = re.compile(r'([\-\.\_\w]+\.tar\.zst)\s')
        dir_content = set(pattern.findall(dir_list))

        correct_content = set(self.origin_dict.keys())
        self.obsolete = dir_content.difference(correct_content)
        self.missing = correct_content.difference(dir_content)

        if self.obsolete == set() and self.missing==set():
            print("[Everything is correct!]")
            self.no_misobs = True
        else:
            print("[Битая Флешка! Стёп, перезалей!]")
            print("Obsolete Archives: ")
            for i in self.obsolete:
                print(i)
            print('\n')
            print("Missing Archives: ")
            for j in self.missing:
                print(j)
            print('\n')


    def find_diff(self, d1, d2):
        diff = {}
        for key in d1:
            if (key in d2 and d1[key] != d2[key]):
                diff[key] = d1[key]
        return diff 


    def errorlist(self):
      self.errors = self.find_diff(self.main_dict, self.origin_dict)

      if self.errors == {}:
          print("[No Errors Found]")
          self.no_error = True
      else:
          print("[Error Found!]")
          for i in self.errors:
              print(i)
          print('\n')


    def archive_fixer(self):
        fix = input("Fix all archives? (yes/no)")

        if fix == 'no':
            print(f"Not fixing archives :(")
        else:
            for obs in self.obsolete:
                os.system(f"rm -rf {obs} && echo '[{obs} Удален]'")
            for mis in self.missing:
                #print(f"[{mis} Копируется]")
                os.system(f"cp -v {self.dw_path}{mis} ./")
            for err in self.errors:
                #print(f"[{err} Копируется]")
                os.system(f"cp -v {self.dw_path}{err} ./")


    def validate(self):
        self.read_info()
        self.val_num()
        self.errorlist()
        if self.no_error == True and self.no_misobs == True:
            print("Тут больше нечего делать")
        else:
            self.archive_fixer()
        

if __name__ == "__main__":
    calculon().validate()
    input("Нажмите Enter для закрытия окна")

