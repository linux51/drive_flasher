import typing
import glob
import os
import logging


def default_searcher():
        """
        Стандартная функция поиска подключенных устройств
        Ищет устройства по пути /dev/ устройства соответствующие патерну sd[b-z]3
        """
        default_path = "/dev/sd[b-z]3"
        paths = glob.glob(default_path)
        return paths


class Mounter:

    """
    Класс инкапсулирующий логику по монтированию usb-флешек в Linux
    """

    def __init__(self, mount_root: str, delete_paths: bool = False, searcher: typing.Callable = None):
        if searcher == None:
            self.searcher = default_searcher
        else:
            self.searcher = searcher
        self.mount_root = mount_root
        self._used_ids = set()
        self._next_id = 0
        self._mounted_paths = []
        self.delete_paths = delete_paths
        logging.basicConfig(level=logging.INFO)

    def get_mounted_paths(self):
        """
        Возвращает массив путей с примонтированными устройствами
        """
        return self._mounted_paths[:]
    
    def _get_next_id(self):
        next_id = self._next_id
        self._used_ids.add(next_id)
        self._next_id += 1
        return next_id

    def _search_devices(self):
        """
        Ищет подключеные устройства при помощи функции поиска
        """
        devices = self.searcher()
        logging.log(logging.INFO, f"найдены следующие устройства: {devices}")
        return devices

    def _create_directory(self, root):
        """
        Создает папку для монтирования usb-флешки
        """
        id = self._get_next_id()
        mount_path = root + f"{id}"
        mkdir_cmd = f"mkdir -p {mount_path}"

        try:
            logging.log(logging.INFO, f"создание папки для монтирования {mount_path}")
            os.system(mkdir_cmd)
        except FileExistsError:
            pass
        return mount_path

    def _delete_directory(self, path):
        """
        Удаляет папку
        """
        #logging.info(f"удаление папки {path}")
        #os.system(f"rm -rf {path}")
        print("RIP Петя")

    def _mount(self, src, dest):
        """
        Монтирует устройства в папки с уникальным идентификатором
        """
        mount_cmd = f"mount {src} {dest}"
        logging.info(f"монтирование устройства {src} в {dest}")
        os.system(mount_cmd)

    def mount(self) -> int:
        """
        Ищет покдлюченые устройства используя функцию поиска searcher
        и монтирует их в папки с уникальным идентификатором
        """
        devices = self._search_devices()
        for dev in devices:
            mount_path = self._create_directory(self.mount_root)
            self._mount(dev, mount_path)
            self._mounted_paths.append(mount_path)

    def update(self):
        """
        Копирует скрипты из локальной папки на флешки
        """
        scr_path = f"../update_scripts/"
        install = f"install.sh"
        dualboot = f"dualboot.sh"
        upd_to_2 = f"upd_to_2.0.sh"
        uivc = f"uivc"
        calc = f"calc-2.0.py"

        for i in self._mounted_paths:
            i += "/root/"
            logging.info(f"копирование инсталляционных скриптов в root")
            os.system(f"cp {scr_path+install} {i}")
            os.system(f"cp {scr_path+dualboot} {i}")
            os.system(f"cp {scr_path+upd_to_2} {i}")
            
        for j in self._mounted_paths:
            j += "/opt/"
            logging.info(f"копирование pppoe в opt")
            os.system(f"cp -R {scr_path+uivc} {j}")

        for k in self._mounted_paths:
            k += "/archive/"
            logging.info(f"копирование {calc} в archive")
            os.system(f"cp {scr_path+calc} {k}")
            
            logging.info(f"Мы сейчас на флешке {k}")
            launch_calculon = input("Открыть починку архивов? (yes/no) ")
            if launch_calculon == 'yes':
                cd = f"cd {k} ; "
                lp = f"dbus-launch gnome-terminal -- python3 {calc} ; "
                cd_back = f"cd - "
                os.system(cd+lp+cd_back)
            else:
                print("Ну ладно...")

    def _umount(self, mount_path):
        """
        Демонтирует usb флешку по пути mount_path
        """
        umount_cmd = f"umount {mount_path}"
        logging.info(f"размонтирование {mount_path}")
        os.system(umount_cmd)


    def umount(self):
        """
        Демонтирует примонтированные устройства
        """
        for path in self._mounted_paths:
            self._umount(path)
        
        if self.delete_paths:
            self._delete_directory(self.mount_root)

